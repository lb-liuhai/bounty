import os
basedir = os.path.abspath(os.path.dirname(__file__))

DATABASE_URL = 'mysql+pymysql://root:@127.0.0.1:3306/bounty?charset=utf8mb4'

ROOT = os.path.abspath(os.path.dirname(__file__))
if os.path.exists(os.path.join(ROOT, 'config_local.py')):
    exec(open(os.path.join(ROOT, 'config_local.py')).read())

class Config:

    SECRET_KEY = os.environ.get('SECRET_KEY') or 'xxxxx'
    # 每次请求结束后自动commit
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    # 如果设置成 True (默认情况)，Flask-SQLAlchemy 将会追踪对象的修改并且发送信号。这需要额外的内存， 如果不必要的可以禁用它
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # 七牛
    QINIU_BUCKET_NAME = 'blog'
    QINIU_ACCESS_KEY = 'xxxx'
    QINIU_SECRET_KEY = 'xxx'
    QINIU_BUCKET_DOMAIN = 'xxx'

    # redis
    REDIS_URL='redis://127.0.0.1:6379/0'

    @staticmethod
    def init_app(app):
        pass


# 开发
class DevelopmentConfig(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('DEV_DATABASE_URL') or DATABASE_URL


# 测试
class TestingConfig(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('TEST_DATABASE_URL') or DATABASE_URL


# 发布
class ProductionConfig(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or DATABASE_URL
    print(SQLALCHEMY_DATABASE_URI)


config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,
    'default': DevelopmentConfig
}