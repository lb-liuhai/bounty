from flask_script import Manager, Shell, Server
from flask_migrate import Migrate, MigrateCommand
from common.database import db
from app import create_app


app = create_app()
manager = Manager(app)
migrate = Migrate(app, db)

manager.add_command('db', MigrateCommand)
manager.add_command('runserver', Server(use_debugger=True))

@manager.command
def create_superuser(name, password):
    """创建管理员用户"""
    if not all([name, password]):
        print('参数不足')
        return

    from werkzeug.security import generate_password_hash
    from app.models import Manger
    m = Manger()
    m.user_name = name
    m.password = generate_password_hash(password)
    m.is_superadmin = True

    try:
        db.session.add(m)
        db.session.commit()
        print("创建成功")
    except Exception as e:
        print(e)
        db.session.rollback()


if __name__ == '__main__':
    manager.run()