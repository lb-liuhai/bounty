from flask import make_response, request
from io import BytesIO
import flask_restful
from flask_restful import Resource, reqparse
from common.utils import validate_picture, create_jwt_token
from app import rd, app
from app.errors import my_abort, my_response
from app.models import (
    User,
)
# 自定义错误
flask_restful.abort = my_abort


class VerifyCodeAPI(Resource):

    def get(self):
        """生成二维码地址，并存下随机串"""
        image, str_code = validate_picture()
        # 将验证码图片以二进制形式写入在内存中，防止将图片都放在文件夹中，占用大量磁盘
        buf = BytesIO()
        image.save(buf, 'jpeg')
        buf_str = buf.getvalue()
        # 把二进制作为response发回前端，并设置首部字段
        response = make_response(buf_str)
        response.headers['Content-Type'] = 'image/gif'
        # 将验证码字符串储存在redis中
        ip = request.remote_addr
        rd.set(ip, str_code)
        rd.expire(ip, 60)
        return response


class UserLoginAPI(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('user_name', type=str, required=True, help='请提交用户名')
        self.parser.add_argument('password', type=str, required=True, help='请提交密码')

    def post(self):
        args = self.parser.parse_args()

        user_name = args.get('user_name')
        password = args.get('password')

        user = User.query_set().filter_by(user_name=user_name).first()
        if not user or not user.check_password(password):
            return my_response(message='用户名不存在或密码错误', status=400)
        
        # 成功返回用户信息及token
        status, jwt_token = create_jwt_token(app, user.id, user.user_name)
        data = {
            'uid': user.id,
            'user_name': user.user_name,
            'real_name': user.real_name,
            'mobile': user.mobile,
            'remark': user.remark,
            'token': jwt_token
        }
        return my_response(data)


class UserRegisterAPI(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('user_name', type=str, required=True, help='请提交用户名')
        self.parser.add_argument('password', type=str, required=True, help='请提交密码')
        self.parser.add_argument('re_password', type=str, required=True, help='请提交确认密码')
        self.parser.add_argument('verify_code', type=str, required=True, help='请提交图形验证码')

    def post(self):
        args = self.parser.parse_args()
        user_name = args.get('user_name')
        password = args.get('password')
        re_password = args.get('re_password')
        verify_code = args.get('verify_code')

        user = User.query_set().filter_by(user_name=user_name).first()
        if user:
            return my_response(message='该手机号用户已存在', status=400)
        if password != re_password:
            return my_response(message='输入密码和确认密码不一致', status=400)

        ip = request.remote_addr
        if not rd.get(ip) or str(rd.get(ip), encoding='utf8') != verify_code:
            return my_response(message='图形验证码错误或已失效', status=400)

        # 创建用户
        user = User()
        user.user_name = user_name
        user.mobile = user_name
        user.auto_password(password)
        user.add()
        # 成功返回用户信息及token
        status, jwt_token = create_jwt_token(app, user.id, user.user_name)
        data = {
            'uid': user.id,
            'user_name': user.user_name,
            'real_name': user.real_name,
            'mobile': user.mobile,
            'remark': user.remark,
            'token': jwt_token
        }
        return my_response(data)


class UserForgetPasswordAPI(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('user_name', type=str, required=True, help='请提交用户名')
        self.parser.add_argument('password', type=str, required=True, help='请提交密码')
        self.parser.add_argument('re_password', type=str, required=True, help='请提交确认密码')
        self.parser.add_argument('verify_code', type=str, required=True, help='请提交图形验证码')

    def post(self):
        args = self.parser.parse_args()
        user_name = args.get('user_name')
        password = args.get('password')
        re_password = args.get('re_password')
        verify_code = args.get('verify_code')

        user = User.query_set().filter_by(user_name=user_name).first()
        if not user:
            return my_response(message='该手机号用户不存在', status=400)
        if password != re_password:
            return my_response(message='输入密码和确认密码不一致', status=400)

        ip = request.remote_addr
        if not rd.get(ip) or str(rd.get(ip), encoding='utf8') != verify_code:
            return my_response(message='图形验证码错误或已失效', status=400)

        # 更新用户
        user.user_name = user_name
        user.mobile = user_name
        user.auto_password(password)
        user.update()

        # 成功返回用户信息及token
        status, jwt_token = create_jwt_token(app, user.id, user.user_name)
        data = {
            'uid': user.id,
            'user_name': user.user_name,
            'real_name': user.real_name,
            'mobile': user.mobile,
            'remark': user.remark,
            'token': jwt_token
        }
        return my_response(data)
