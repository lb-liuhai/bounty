from flask import Blueprint
from flask_restful import Api
from .auth import (
    VerifyCodeAPI,
    UserLoginAPI,
    UserRegisterAPI,
    UserForgetPasswordAPI
)
api_bp = Blueprint('api', __name__, url_prefix='/api')
api = Api(api_bp)

# 用户相关
api.add_resource(VerifyCodeAPI, '/verify_code/', endpoint = 'verify_code')
api.add_resource(UserLoginAPI, '/v1/user/login/', endpoint = 'user-login')
api.add_resource(UserRegisterAPI, '/v1/user/register/', endpoint = 'user-register')
api.add_resource(UserForgetPasswordAPI, '/v1/user/forget_password/', endpoint = 'user-forget-password')