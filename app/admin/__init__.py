from flask_admin import Admin
from .views import (
    MyAdminIndexView,
    BookAdminView,
    UserAdminView,
)
from ..models import (
    User,
    db,
    Book
)

admin = Admin(name="后台管理系统", index_view=MyAdminIndexView(), base_template='admin/base.html')

# add views
admin.add_view(BookAdminView(Book, db.session, name='教材管理'))
admin.add_view(UserAdminView(User, db.session, name='用户管理'))

