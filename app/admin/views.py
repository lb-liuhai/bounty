from flask import Blueprint, render_template, abort, flash, redirect, url_for, request
import flask_login as login
from .forms import LoginForm, UserForm, BookForm
from ..models import User, Manger, Book, Category
from sqlalchemy import desc, asc
from flask_admin import expose, BaseView, AdminIndexView
from flask_admin.contrib.sqla import ModelView


class MyAdminIndexView(AdminIndexView):

    @expose('/')
    def index(self):
        if not login.current_user.is_authenticated:
            return redirect(url_for('.login_view'))

        return self.render('/admin/index.html')

    @expose('/login/', methods=('GET', 'POST'))
    def login_view(self):

        form = LoginForm(request.form)
        if form.validate_on_submit():
            manger = Manger.query.filter_by(user_name=form.user_name.data).first()
            if manger is not None and manger.verify_password(form.password.data):
                login.login_user(manger)
            else:
                flash("账号或密码错误")
        if login.current_user.is_authenticated:
            return redirect(url_for('.index'))

        self._template_args['form'] = form
        return self.render('/admin/auth/login.html')

    @expose('/logout/')
    def logout_view(self):
        login.logout_user()
        return redirect(url_for('.index'))


class BookAdminView(ModelView):
    
    def is_accessible(self):
        return login.current_user.is_authenticated

    @expose('list/')
    def list(self):
        name = request.args.get('name', '')
        page = request.args.get('page', 1, type=int)

        books = Book.query_set()

        # 查询条件
        if name:
            books = books.filter(
                Book.name.ilike("%"+name+"%")
            )

        pagination = books.order_by(
            desc(Book.create_time)
        ).paginate(page, per_page=10, error_out=False)
        clients = pagination.items

        return self.render('/admin/book/list.html', 
            module='book',
            name=name,
            clients=clients,
            page=page,
            pagination=pagination
        )

    @expose('add/', methods=['GET', 'POST'])
    def add(self):
        page = request.args.get('page', 1, type=int)
        form = BookForm()
        if form.validate_on_submit():

            print(form)
            # book = Book()
            # user.user_name = form.user_name.data
            # user.real_name = form.real_name.data
            # user.mobile = form.mobile.data
            # user.remark = form.remark.data
            # user.user_type = 2
            # user.auto_password()
            # user.add()

            return redirect(url_for('.list', page=page))

        return self.render('/admin/book/add.html', 
            module='book',
            page=page,
            form=form
        )

    @expose('add/<int:id>/', methods=['GET', 'POST'])
    def edit(self, id):
        page = request.args.get('page', 1, type=int)
        form = UserForm(request.form)

        if form.validate_on_submit():
            user = User()
            user.user_name = form.user_name.data
            user.real_name = form.real_name.data
            user.mobile = form.mobile.data
            user.remark = form.remark.data
            user.user_type = 2
            user.auto_password()
            user.add()

            return redirect(url_for('.list', page=page))

        return self.render('/admin/user/add.html', 
            module='user',
            page=page,
            form=form
        )

    @expose('del/<int:id>/')
    def delete(self, id):
        page = request.args.get('page', 1, type=int)

        user = User.query_set().filter_by(id=id).first()
        if user:
            user.drop()

        return redirect(url_for('.list', page=page))


class UserAdminView(ModelView):
    
    def is_accessible(self):
        return login.current_user.is_authenticated

    @expose('list/')
    def list(self):
        user_name = request.args.get('user_name', '')
        page = request.args.get('page', 1, type=int)

        users = User.query_set()

        # 查询条件
        if user_name:
            users = users.filter(
                User.user_name.ilike("%"+user_name+"%")
            )

        pagination = users.order_by(
            desc(User.create_time)
        ).paginate(page, per_page=10, error_out=False)
        clients = pagination.items

        return self.render('/admin/user/list.html', 
            module='user',
            user_name=user_name,
            clients=clients,
            page=page,
            pagination=pagination
        )

    @expose('add/', methods=['GET', 'POST'])
    def add(self):
        page = request.args.get('page', 1, type=int)
        form = UserForm(request.form)

        if form.validate_on_submit():
            user = User()
            user.user_name = form.user_name.data
            user.real_name = form.real_name.data
            user.mobile = form.mobile.data
            user.remark = form.remark.data
            user.user_type = 2
            user.auto_password()
            user.add()

            return redirect(url_for('.list', page=page))

        return self.render('/admin/user/add.html', 
            module='user',
            page=page,
            form=form
        )

    @expose('del/<int:id>/')
    def delete(self, id):
        page = request.args.get('page', 1, type=int)

        user = User.query_set().filter_by(id=id).first()
        if user:
            user.drop()

        return redirect(url_for('.list', page=page))