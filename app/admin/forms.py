from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired, FileAllowed
from wtforms import (
    StringField,
    PasswordField,
    TextAreaField,
    SelectField,
    widgets,
)
from wtforms.validators import (
    DataRequired,
    Required,
    Length,
    Email,
    EqualTo,
    ValidationError
)
from ..models import Manger, User, Category


class LoginForm(FlaskForm):
    user_name = StringField('管理员账号', validators=[DataRequired()])
    password = PasswordField('密码', validators=[DataRequired()])

    def validate_login(self):
        manager = self.get_manager()
        
        if manager is None:
            raise ValidationError('Invalid user')

        # we're comparing the plaintext pw with the the hash from the db
        if not manager.verify_password(self.password.data):
            # to compare plain text passwords use
            # if user.password != self.password.data:
            raise ValidationError('Invalid password')

    def get_manager(self):
        return Manger.query_set().filter_by(user_name=self.user_name.data).first()


class UserForm(FlaskForm):
    user_name = StringField(
        '用户名',
        validators=[DataRequired('用户名不能为空'), ],
        widget=widgets.TextInput(),
        render_kw={"class": "form-control"}
    )
    real_name = StringField(
        '昵称',
        validators=[DataRequired('昵称不能为空')],
        widget=widgets.TextInput(),
        render_kw={"class": "form-control"}
    )
    mobile = StringField(
        '手机号',
        validators=[DataRequired('手机号不能为空'), Length(11, message='手机号必须11位')],
        widget=widgets.TextInput(),
        render_kw={"class": "form-control"}
    )
    remark = TextAreaField(
        '备注',
        validators=[],
        render_kw={"class": "form-control", "rows": 4}
    )

    def validate_user_name(self, field):
        user = User.query_set().filter_by(user_name=field.data).first()
        if user:
            raise ValidationError('用户：%s已存在' % user.user_name)

    def validate_mobile(self, field):
        user = User.query_set().filter_by(mobile=field.data).first()
        if user:
            raise ValidationError('手机号：%s已存在' % user.mobile)


class BookForm(FlaskForm):

    name = StringField(
        '教材名称',
        validators=[DataRequired('教材名称不能为空'), ],
        widget=widgets.TextInput(),
        render_kw={"class": "form-control"}
    )
    choices = [(0, '请选择类目')]
    category_id = SelectField(
        '所属类目',
        coerce=int,
        render_kw={"class": "form-control"},
        validators=[Required('所属类目不能为空')],
        choices=choices
    )
    cover = FileField(
        '封面图片',
        validators=[
            # 文件必须选择;
            FileRequired('请上传封面图片'),
            # 指定文件上传的格式;
            FileAllowed(['jpg','jpeg','png','gif'], '请选择图片格式作为封面')
        ],
        render_kw={"class": "form-control"},
    )
    down = FileField(
        "下载资料",
        render_kw={"class": "form-control"},
        validators=[
            # 文件必须选择;
            FileRequired('请上传下载资料'),
            # 指定文件上传的格式;
            FileAllowed(['pdf', 'ppt'], '只接收pdf和ppt格式的文件')
        ]
    )
    archive = FileField(
        "模考题库",
        render_kw={"class": "form-control"},
        validators=[
            # 文件必须选择;
            FileRequired('请上传模考题库'),
            # 指定文件上传的格式;
            FileAllowed(['rar'], '请上传压缩文件上传题库')
        ]
    )
    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        # 添加类目选择
        categories = Category.query_set().order_by(Category.no)
        for c in categories:
            tup = (c.id, c.name)
            if tup not in self.choices:
                self.choices.append((c.id, c.name))