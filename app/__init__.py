from common.database import db
from common.qiniu_storage import Qiniu
from flask_login import LoginManager
from config import config
from flask import Flask
app = Flask(__name__, static_url_path='')

from flask_redis import FlaskRedis

login_manager = LoginManager()
# redis
rd = FlaskRedis(app)

def create_app():
    config_name = 'default'
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)

    login_manager.login_view = 'admin.login_view'
    login_manager.init_app(app)

    from .admin import admin
    admin.init_app(app)
    # 接口
    from .api import api_bp
    app.register_blueprint(api_bp)

    db.init_app(app)

    # 注册蓝图
    from app.backend import backend
    from app.backend import api
    api.init_app(app)
    app.register_blueprint(backend)

    qiniu = Qiniu()
    qiniu.init_app(app)

    return app


@app.teardown_request
def shutdown_session(exception=None):
    db.session.remove()