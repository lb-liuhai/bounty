
from flask_restful import abort


class ResponseCode:
    MESSAGE = '处理成功！'
    SUCCESS = 200
    FAILURE = 400


def my_response(data=None, message=ResponseCode.MESSAGE, status=ResponseCode.SUCCESS):
    return {
        'message': message,
        'status': status,
        'data': data
    }

def my_abort(http_status_code, *args, **kwargs):
    if http_status_code == 400:
        # 重定义400返回参数
        try:
            message = kwargs.get('message')
            message = list(message.values())[0]
        except Exception as e:
            message = kwargs.get('message')

        abort(400, **my_response(message=message, status=http_status_code))

    abort(http_status_code)