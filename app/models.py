from flask_login import UserMixin, AnonymousUserMixin
from sqlalchemy import (
    Column,
    Integer,
    String,
    Float,
    DECIMAL,
    Boolean,
    Enum,
    DateTime,
    Text,
    func,
    ForeignKey
)
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.mysql import TINYINT
from common.database import db
from werkzeug.security import generate_password_hash, check_password_hash
from app import login_manager
import datetime


class BaseModule(db.Model):
    """公共类"""
    __tablename__ = 'base'
    id = Column(Integer, primary_key=True)
    create_time = Column(DateTime, server_default=func.now(), comment='创建时间')
    update_time = Column(DateTime, server_default=func.now(), onupdate=func.now(), comment='修改时间')
    is_del = Column(Boolean, default=False, comment='是否删除')

    @classmethod
    def query_set(cls):
        return cls.query.filter_by(is_del=False)

    def add(self):
        db.session.add(self)
        db.session.commit()

    def update(self):
        db.session.commit()

    def drop(self):
        db.session.delete(self)
        db.session.commit()

    def delete(self):
        self.is_del = True
        db.session.commit()


class User(BaseModule):
    """用户"""
    __tablename__ = 'user'
    id = Column(Integer, ForeignKey('base.id', ondelete='CASCADE'), primary_key=True)
    user_type = Column(TINYINT, default=1, comment='用户类型')
    user_name = Column(String(50), unique=True, comment='用户名')
    real_name = Column(String(50), comment='昵称')
    mobile = Column(String(50), unique=True, comment='手机号')
    email = Column(String(120), comment='邮箱')
    remark = Column(Text, comment='备注')
    password = Column(Text, comment='密码')
    password_text = Column(Text, comment='密码明文')

    def __init__(self, user_name=None, user_type=None, email=None):
        self.user_name = user_name
        self.email = email

    def __repr__(self):
        return '<User %r>' % (self.user_name)

    def auto_password(self, password_text = '123456'):
        """生成密码"""
        self.password_text = password_text
        self.password = generate_password_hash(self.password_text)

    def check_password(self, password):
        """校验密码"""
        return check_password_hash(self.password, password)


class Manger(UserMixin, BaseModule):
    """管理员"""
    __tablename__ = 'manger'
    id = Column(Integer, ForeignKey('base.id', ondelete='CASCADE'), primary_key=True)
    user_name = Column(String(50), unique=True, comment='用户名')
    is_superadmin = Column(Boolean, default=False, comment='是否是超级用户')
    password = Column(String(255), comment='密码')

    def is_authenticated(self):
        return True

    def verify_password(self, password):
        return check_password_hash(self.password, password)

    def __repr__(self):
        return '<User %r>' % (self.user_name)

    def to_json(self):
        return {"id": self.id,
                "user_name": self.user_name,
                "create_time": str(self.create_time)}

    @staticmethod
    def generate_password(password):
        return generate_password_hash(password)

    def check_password(self, old_password):
        return check_password_hash(self.password, old_password)


@login_manager.user_loader
def load_user(user_id):
    print('user_id', user_id)
    return db.session.query(Manger).get(user_id)


class Category(BaseModule):
    """类目"""
    __tablename__ = 'category'
    id = Column(Integer, ForeignKey('base.id', ondelete='CASCADE'), primary_key=True)
    no = Column(Integer)
    name = Column(String(64), unique=True, comment='名称')
    remark = Column(Text, comment='备注')

    def __repr__(self):
        return '<Category %r>' % self.name

    def __str__(self):
        return self.name


class Book(BaseModule):
    """教材"""
    __tablename__ = 'book'
    id = Column(Integer, ForeignKey('base.id', ondelete='CASCADE'), primary_key=True)
    name = Column(String(64), unique=True, comment='名称')
    cover = Column(String(300), comment='封面')
    down = Column(String(300), comment='下载资料')
    archive = Column(String(300), comment='模考题库压缩包')
    exam = Column(String(300), comment='题库')
    category_id = Column(Integer, ForeignKey("category.id", ondelete='CASCADE'), comment='类目')
    category = relationship("Category", foreign_keys=[category_id])

    def __repr__(self):
        return '<Book %r>' % self.name

    @property
    def cover_url(self):
        return self.cover

    @property
    def down_url(self):
        return self.down

    @property
    def archive_url(self):
        return self.archive


class Course(BaseModule):
    """课程"""
    __tablename__ = 'course'
    id = Column(Integer, ForeignKey('base.id', ondelete='CASCADE'), primary_key=True)
    name = Column(String(64), unique=True, comment='名称')
    price = Column(Float, comment='价格')
    category_id = Column(Integer, ForeignKey("category.id", ondelete='CASCADE'), comment='类目')
    category = relationship("Category", foreign_keys=[category_id])
    content = Column(Text, comment='简介')

    def __repr__(self):
        return '<Course %r>' % self.name


class Tag(db.Model):
    """标签"""
    __tablename__ = 'tag'
    id = Column(Integer, primary_key=True)
    name = Column(String(50), unique=True, nullable=False, comment='名称')

    def __repr__(self):
        return '<Tag %r>' % self.name


class Video(BaseModule):
    """视频"""
    __tablename__ = 'video'
    id = Column(Integer, ForeignKey('base.id', ondelete='CASCADE'), primary_key=True)
    name = Column(String(64), unique=True, comment='名称')
    cover = Column(String(300), comment='封面')
    price = Column(Float, comment='价格')
    url = Column(String(300), comment='视频地址')
    teacher = Column(String(64), comment='讲师')
    course_id = Column(Integer, ForeignKey("course.id", ondelete='CASCADE'), comment='课程')
    click_volume = Column(Integer, comment='点击量')
    course = relationship("Course", foreign_keys=[course_id])
    tag_id = Column(Integer, ForeignKey("tag.id"), comment='标签')
    tag = relationship("Tag", foreign_keys=[tag_id])
    content = Column(Text, comment='简介')

    def __repr__(self):
        return '<Video %r>' % self.name


class Comment(BaseModule):
    """评论"""
    __tablename__ = 'comment'
    id = Column(Integer, ForeignKey('base.id', ondelete='CASCADE'), primary_key=True)
    content = Column(Text, comment='评论内容')
    video_id = Column(Integer, ForeignKey("video.id", ondelete='CASCADE'), comment='视频')
    video = relationship("Video", foreign_keys=[video_id])
    user_id = Column(Integer, ForeignKey("user.id", ondelete='CASCADE'), comment='用户')
    user = relationship("User", foreign_keys=[user_id])

    def __repr__(self):
        return '<Comment %r>' % self.content


class Order(BaseModule):
    """订单"""
    __tablename__ = 'order'
    id = Column(Integer, ForeignKey('base.id', ondelete='CASCADE'), primary_key=True)
    pay = Column(TINYINT, default=1, comment='付款方式')
    price = Column(Float, comment='价格')
    course_id = Column(Integer, ForeignKey("course.id", ondelete='CASCADE'), comment='课程')
    course = relationship("Course", foreign_keys=[course_id])
    remark = Column(Text, default="", comment='备注')
    user_id = Column(Integer, ForeignKey("user.id", ondelete='CASCADE'), comment='用户')
    user = relationship("User", foreign_keys=[user_id])
    category_id = Column(Integer, ForeignKey("category.id"), comment='类目')
    category = relationship("Category", foreign_keys=[category_id])

    def __repr__(self):
        return '<Order %r>' % self.remark

    def to_json(self):
        return {"id": self.id,
                "pay": self.pay,
                "price": self.price,
                "user_id": self.user_id,
                "user_name": self.user.user_name,
                "course_id": self.course_id,
                "course_name": self.course.name,
                "remark": self.remark,
                "category_id": self.category.id,
                "category_name": self.category.name,
                "is_del": self.is_del,
                "create_time": str(self.create_time)}



class QuestionAndAnswer(BaseModule):
    """问答中心"""
    __tablename__ = 'question_and_answer'
    id = Column(Integer, ForeignKey('base.id', ondelete='CASCADE'), primary_key=True)
    no = Column(Integer, comment='序号')
    question = Column(Text, comment='问题')
    answer = Column(Text, comment='答案')

    def __repr__(self):
        return '<QuestionAndAnswer %r>' % self.content