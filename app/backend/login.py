from flask_restful import Resource, request
from app.models import Manger
from werkzeug.security import check_password_hash


class Login(Resource):
    def post(self):
        args = request.form
        username = args['username']
        password = args['password']
        result = Manger.query(password).filter(Manger.username==username).first_or_404()
        if result or check_password_hash(result, password):
            pass
        # 生成token并返回对应信息
        pass