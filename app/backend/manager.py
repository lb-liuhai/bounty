from flask_restful import Resource, request
from app.models import (
    Manger,
    BaseModule,
)
from common.database import db

class Manager(Resource):
    def get(self, manager_id):
        manager = db.session.query(Manger).filter(BaseModule.is_del==0, Manger.id==manager_id).first()
        if not manager:
            return {"msg": "用户不存在"}, 400
        return Manger.to_json(manager), 200

    def put(self, manager_id):
        username = request.form['username']
        password = request.form['password']
        old_password = request.form['old_password']
        manager = db.session.query(Manger).filter(BaseModule.is_del==0, Manger.id==manager_id).first()
        if not manager:
            return {"msg": "用户不存在"}, 400
        if not manager.check_password(old_password):
            return {"msg": "原始密码错误"}, 400
        if username and manager.user_name != username:
            manager_exist = db.session.query(Manger).filter(BaseModule.is_del == 0,
                                                            Manger.user_name == username).first()
            if manager_exist:
                return {"msg": "用户已存在"}, 400
        if password:
            manager.password = Manger.generate_password(password)
        manager.user_name = username
        db.session.add(manager)
        db.session.commit()
        return {}, 200

    def delete(self, manager_id):
        manager = db.session.query(Manger).filter(BaseModule.is_del==0, Manger.id==manager_id).first()
        if not manager:
            return {"msg": "用户不存在"}, 400
        manager.is_del = True
        db.session.add(manager)
        db.session.commit()
        return {}, 204


class ManagerList(Resource):
    def get(self):
        managers = db.session.query(Manger).filter(BaseModule.is_del==0).order_by(Manger.id).all()
        result = [Manger.to_json(manager) for manager in managers]
        return {"managers": result}

    def post(self):
        username = request.form['username']
        password = request.form['password']
        manager_exist = db.session.query(Manger).filter(Manger.is_del==0, Manger.user_name==username).first()
        if manager_exist:
            return {"msg": "用户已存在"}, 400
        manger = Manger(user_name= username,  password=Manger.generate_password(password))
        db.session.add(manger)
        db.session.commit()
        return {}, 201

