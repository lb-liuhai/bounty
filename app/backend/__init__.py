from flask import Blueprint
from flask_restful import Api
from . import (
    manager,
    order,
    )

backend = Blueprint('backend', __name__)
api = Api()
api.add_resource(manager.ManagerList, '/managers')
api.add_resource(manager.Manager, '/manager/<manager_id>')
api.add_resource(order.OrderList, '/orders')
api.add_resource(order.Order, '/order/<order_id>')
