from common.database import db
from flask_restful import Resource, request
from app.models import (
    Order as OrderModule,
)


class Order(Resource):
    def get(self, order_id):
        order = db.session.query(OrderModule).filter(OrderModule.id==order_id, OrderModule.is_del==False).first()
        return OrderModule.to_json(order), 200

    def delete(self, order_id):
        order = db.session.query(OrderModule).filter(OrderModule.id==order_id).first()
        order.is_del = True
        db.session.add(order)
        db.session.commit()
        return {}, 204


class OrderList(Resource):
    def get(self):
        limit = request.args.get("limit",10)
        offset = request.args.get("offset")
        orders = db.session.query(OrderModule).filter(OrderModule.is_del==0).limit(limit).offset(offset).all()
        order_count = len(db.session.query(OrderModule).filter(OrderModule.is_del==0).all())
        order_list = [OrderModule.to_json(order) for order in orders]
        return {"order_count": order_count, "order_list": order_list}, 200


    def post(self):
        order = OrderModule()
        # for k, v in request.form.to_dict().items():
        #     order.price = v
        order.user_id = request.form['user_id']
        order.category_id = request.form['category_id']
        order.course_id = request.form['course_id']
        order.pay = request.form['pay']
        order.price = request.form['price']
        order.remark = request.form.get("mark", "")
        db.session.add(order)
        db.session.commit()
        return {}, 201