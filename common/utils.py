from PIL import Image, ImageFont, ImageDraw, ImageFilter
import random
import jwt
import time
import os


def create_jwt_token(app, user_id, user_name):
    if not user_id or not user_name:
        return False, None

    payload = {
         "iat": int(time.time()),
         "exp": int(time.time()) + 86400 * 7,
         "sub": user_id,
         "username": user_name,
         "scopes": ['open']
    }
    token = jwt.encode(payload, app.config['SECRET_KEY'], algorithm='HS256')
    return True, str(token, encoding='utf8')

def verify_jwt_token(app, token):
    #  如果在生成token的时候使用了aud参数，那么校验的时候也需要添加此参数
    payload = jwt.decode(token, app.config['SECRET_KEY'], algorithms=['HS256'])
    if payload:
        return True, token
    return False, token

def validate_picture():
    res_path = os.path.abspath('./')
    total = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ012345789'
    # 图片大小130 x 50
    width = 130
    heighth = 50
    # 先生成一个新图片对象
    im = Image.new('RGB',(width, heighth), 'white')
    # 设置字体
    font = ImageFont.truetype('%s/app/static/fonts/FreeSans.ttf' % res_path, 40)
    # 创建draw对象
    draw = ImageDraw.Draw(im)
    str_code = ''
    # 输出每一个文字
    for item in range(5):
        text = random.choice(total)
        str_code += text
        draw.text((5+random.randint(4,7)+20*item,5+random.randint(3,7)), text=text, fill='black',font=font)

    # 划几根干扰线
    for num in range(8):
        x1 = random.randint(0, width/2)
        y1 = random.randint(0, heighth/2)
        x2 = random.randint(0, width)
        y2 = random.randint(heighth/2, heighth)
        draw.line(((x1, y1),(x2,y2)), fill='black', width=1)

    # 模糊下,加个帅帅的滤镜～
    im = im.filter(ImageFilter.FIND_EDGES)
    return im, str_code